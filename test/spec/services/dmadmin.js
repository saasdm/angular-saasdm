/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';
describe('service for dmadmin', function() {
	var $dmadmin;
	beforeEach(module('pluf.dm'));
	beforeEach(inject(function(_$dmadmin_) {
		$dmadmin = _$dmadmin_;
	}));

	it('Space method is undefined', function() {
		expect($dmadmin.space).toBeDefined();
	});
});
