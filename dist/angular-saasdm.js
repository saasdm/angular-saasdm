/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * @ngdoc overview
 * @name angularSaasdmApp
 * @description # angularSaasdmApp
 * 
 * Main module of the application.
 */
angular.module('pluf.dm', [ 'pluf' ]);

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('pluf.dm')

/**
 * @ngdoc factory
 * @name DmAsset
 * @description # DmAsset Controller of the angularSaasdmApp
 */

.factory('DmAsset', function(PObject, $pluf) {
	var dmAsset = function() {
		PObject.apply(this, arguments);
	};
	
	dmAsset.prototype = new PObject();

	dmAsset.prototype.update = $pluf.createUpdate({
		method : 'POST',
		url : '/api/dm/asset/:id',
	});

	dmAsset.prototype.delete = $pluf.createDelete({
		method : 'DELETE',
		url : '/api/dm/asset/:id'
	});
	return dmAsset;
});
/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('pluf.dm')
/**
 * @ngdoc function
 * @name DmLink
 * @description # DmLink Controller of the angularSaasdmApp
 */
.factory('DmLink', function(PObject, $pluf) {

	var dmLink = function() {
		PObject.apply(this, arguments);
	};
	dmLink.prototype = new PObject();

	// adds new methods
	dmLink.prototype.update = $pluf.createUpdate({
		method : 'POST',
		url : '/api/dm/link/:id',
	});

	dmLink.prototype.delete = $pluf.createDelete({
		method : 'DELETE',
		url : '/api/dm/link/:id'
	});
	return dmLink;
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('pluf.dm')
/**
 * @ngdoc function
 * @name DmPlan
 * @description # this section about disaster management
 */
.factory('DmPlan', function(PObject, $pluf) {
	var dmPlan = function() {
		PObject.apply(this, arguments);
	};
	dmPlan.prototype = new PObject();

	dmPlan.prototype.update = $pluf.createUpdate({
		method : 'POST',
		url : '/api/dm/plan/:id'
	});

	dmPlan.prototype.delete = $pluf.createDelete({
		method : 'DELETE',
		url : '/api/dm/plan/:id'
	});

	return dmPlan;
});
/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('pluf.dm')

/**
 * @ngdoc function
 * @name DmPlan
 * @description #
 */
.factory('DmPlanTemplate', function(PObject, $pluf) {
	var dmplantemplate = function() {
		PObject.apply(this, arguments);
	};
	dmplantemplate.prototype = new PObject();
	dmplantemplate.prototype.update =$pluf.createUpdate({
		method : 'POST',
		url : '/api/dm/plantemplate/:id' 
	});
	dmplantemplate.prototype.delete = $pluf.createDelete({
		method : 'DELETE',
		url : '/api/dm/plantemplate/:id'
	});
	return dmplantemplate;
});
/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('pluf.dm')

/**
 * @ngdoc factory
 * @name sdpAsset
 * @description # sdpAsset Controller of the angularSaasdmApp
 */

.factory('SdpAsset', function(PObject, PObjectFactory, $injector, $q, $pluf, $httpParamSerializerJQLike, $http) {
    
    	/**
	 * سازنده برای تولید SdpAsset ها
	 */
        var _assetFactory = new PObjectFactory(function(data) {
            if(!this.sdpAssetInjector){
        	this.sdpAssetInjector = $injector.get('SdpAsset');
            }
            return new this.sdpAssetInjector(data);
        });
    
	var sdpAsset = function() {
		PObject.apply(this, arguments);
	};
	
	sdpAsset.prototype = new PObject();

	sdpAsset.prototype.update = $pluf.createUpdate({
		method : 'POST',
		url : '/api/sdp/asset/:id',
	});
	
	sdpAsset.prototype.updateFileOfAsset = function(file) {
		var formData = new FormData();
		var objectData = this;
		for(var key in objectData){
			if(key === 'id'){
				continue;
			}
			if(objectData[key]){
				formData.append(key, objectData[key]);
			}
		}
		formData.append('file', file);
		return $http.post('/api/sdp/asset/' + this.id, formData, {
			transformRequest : angular.identity,
			headers : {
				'Content-Type' : undefined
			}
		})//
		.then(function(res) {
			PObject.apply(this, res);
			return res;
		});
	};

	sdpAsset.prototype.delete = $pluf.createDelete({
		method : 'DELETE',
		url : '/api/sdp/asset/:id'
	});
	
	sdpAsset.prototype.deleteRelationWith = function(asset){
		return $pluf.createDelete({
			method: 'DELETE',
			url: '/api/sdp/asset/' + this.id + '/relation/' + asset.id
		})();
	};
	
	sdpAsset.prototype.getParent = $pluf.createGet({
		method: 'GET',
		url : '/api/sdp/asset/:parent'
	}, _assetFactory);
	
	sdpAsset.prototype.hasParent = function(){
	    return this.parent;
	};
	return sdpAsset;
});
/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('pluf.dm')

/**
 * @ngdoc factory
 * @name sdpAssetRelation
 * @description # sdpAssetRelation Controller of the angularSaasdmApp
 */

.factory('SdpAssetRelation', function(PObject, PObjectFactory, PObjectCache, SdpAsset, $injector, $q, $pluf) {
    
	var _assetCache = new PObjectCache(function(data) {
		return new SdpAsset(data);
	});

	var sdpAssetRelation = function() {
		PObject.apply(this, arguments);
	};
	
	sdpAssetRelation.prototype = new PObject();

	sdpAssetRelation.prototype.update = $pluf.createUpdate({
		method : 'POST',
		url : '/api/sdp/assetrelation/:id',
	});

	sdpAssetRelation.prototype.delete = $pluf.createDelete({
		method : 'DELETE',
		url : '/api/sdp/assetrelation/:id'
	});
	
	sdpAssetRelation.prototype.getStart = $pluf.createGet({
		method: 'GET',
		url : '/api/sdp/asset/:start'
	}, _assetCache);
	
	sdpAssetRelation.prototype.getEnd = $pluf.createGet({
		method: 'GET',
		url : '/api/sdp/asset/:end'
	}, _assetCache);
	
	return sdpAssetRelation;
});
/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('pluf.dm')
/**
 * @ngdoc function
 * @name sdpCategory
 * @description # sdpCategory factory of the angularSaasdmApp
 */
.factory('SdpCategory', function(PObject, PObjectCache, SdpAsset, PObjectFactory, $injector, $pluf) {

        var _assetCache = new PObjectCache(function(data) {
    		return new SdpAsset(data);
        });
    
        /**
	 * سازنده برای تولید SdpAsset ها
	 */
        var _categoryFactory = new PObjectFactory(function(data) {
            if(!this.sdpCategoryInjector){
        	this.sdpCategoryInjector = $injector.get('SdpCategory');
            }
            return new this.sdpCategoryInjector(data);
        });
        
	var sdpCategory = function() {
		PObject.apply(this, arguments);
	};
	sdpCategory.prototype = new PObject();

	
	sdpCategory.prototype.update = $pluf.createUpdate({
		method : 'POST',
		url : '/api/sdp/category/:id'
	});

	sdpCategory.prototype.delete = $pluf.createDelete({
		method : 'DELETE',
		url : '/api/sdp/category/:id'
	});
	
	sdpCategory.prototype.getParent = $pluf.createGet({
		method: 'GET',
		url : '/api/sdp/category/:parent'
	}, _categoryFactory);
	
	sdpCategory.prototype.hasParent = function(){
	    return this.parent;
	};
	
	sdpCategory.prototype.assets = $pluf.createFind({
		method : 'GET',
		url : '/api/sdp/category/:id/asset/find',
	}, _assetCache);
	
	sdpCategory.prototype.addAsset = function(asset){
	    return $pluf.createNew({
		method : 'POST',
		url: '/api/sdp/category/'+this.id+'/asset/'+asset.id
	    }, _assetCache)({/*empty Object*/});
	};
	
	sdpCategory.prototype.removeAsset = function(asset){
	    return $pluf.createDelete({
		method : 'DELETE',
		url: '/api/sdp/category/'+this.id+'/asset/'+asset.id
	    })();
	};
	
	return sdpCategory;
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('pluf.dm')
/**
 * @ngdoc function
 * @name sdpLink
 * @description # sdpLink Controller of the angularSaasdmApp
 */
.factory('SdpLink', function(PObject, PReceipt, PObjectFactory, $injector, $pluf) {

	var _receiptFactory = new PObjectFactory(function(data) {
		return new PReceipt(data);
    });
	
	var _linkFactory = new PObjectFactory(function(data) {
	    if(!this.sdpAssetInjector){
	    	this.sdpLinkInjector = $injector.get('SdpLink');
	    }
	    return new this.sdpLinkInjector(data);
	});
	
	var sdpLink = function() {
		PObject.apply(this, arguments);
	};
	sdpLink.prototype = new PObject();

	// adds new methods
	sdpLink.prototype.update = $pluf.createUpdate({
		method : 'POST',
		url : '/api/sdp/link/:id',
	});

	sdpLink.prototype.delete = $pluf.createDelete({
		method : 'DELETE',
		url : '/api/sdp/link/:id'
	});
	
	sdpLink.prototype.pay = $pluf.createNew({
		method : 'POST',
		url : '/api/sdp/link/:id/pay'
	}, _receiptFactory);
		
	sdpLink.prototype.activate = $pluf.createGet({
		method : 'GET',
		url : '/api/sdp/link/:id/activate'
	}, _linkFactory);
	
// function(backend, callbackUrl){
// if(!backend.id){
// // TODO: Hadi, show invalid gate error.
// }
// var req = {
// url : '/api/sdp/link/' + this.id + '/pay',
// method : 'POST'
// };
// req.headers = {
// 'Content-Type' : 'application/x-www-form-urlencoded'
// };
// var data = {
// backend : backend.id,
// callback : callbackUrl,
// 'XDEBUG_SESSION_START' : 'ECLIPSE_DBGP'
// };
// req.data = $httpParamSerializerJQLike(data);
// return $http(req)//
// .then(function(res) {
// return res.data;
// });
// };
	
	sdpLink.prototype.isActive = function(){
		return this.active === true;
	};
	
	return sdpLink;
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('pluf.dm')
/**
 * @ngdoc function
 * @name sdpTag
 * @description # sdpTag Controller of the angularSaasdmApp
 */
.factory('SdpTag', function(PObject, PObjectCache, SdpAsset, $pluf) {

        var _assetCache = new PObjectCache(function(data) {
    		return new SdpAsset(data);
        });
    
	var sdpTag = function() {
		PObject.apply(this, arguments);
	};
	sdpTag.prototype = new PObject();

	sdpTag.prototype.update = $pluf.createUpdate({
		method : 'POST',
		url : '/api/sdp/tag/:id',
	});

	sdpTag.prototype.delete = $pluf.createDelete({
		method : 'DELETE',
		url : '/api/sdp/tag/:id'
	});
	
	sdpTag.prototype.assets = $pluf.createFind({
		method : 'GET',
		url : '/api/sdp/tag/:id/asset/find',
	}, _assetCache);
	
	sdpTag.prototype.addAsset = function(asset){
	    return $pluf.createNew({
		method : 'POST',
		url: '/api/sdp/tag/'+this.id+'/asset/'+asset.id
	    }, _assetCache)();
	};
	
	sdpTag.prototype.removeAsset = function(asset){
	    return $pluf.createDelete({
		method : 'DELETE',
		url: '/api/sdp/tag/'+this.id+'/asset/'+asset.id
	    })();
	};
	
	return sdpTag;
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('pluf.dm')
/**
 * 
 */
.service('$dm', function($pluf, PObjectCache, DmLink, DmAsset, DmPlan, DmPlanTemplate) {

	var _linkCache = new PObjectCache(function(data) {
		return new DmLink(data);
	});
	var _assetCache = new PObjectCache(function(data) {
		return new DmAsset(data);
	});
	var _planCache = new PObjectCache(function(data) {
		return new DmPlan(data);
	});
	var _planTemplateCache = new PObjectCache(function(data) {
		return new DmPlanTemplate(data);
	});

	/**
	 * لینک معادل با شناسه ورودی را تعیین می‌کند
	 * 
	 * توی این پیاده سازی لینک ورودی به سرور ارسال می‌شه و نتیجه به دست آمده به
	 * صورت یک موجودیت لینک ایجاد می‌شه.
	 * 
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $dm.link('linkId').then(function(link) {
	 * 	// Do something with link
	 * 	});
	 * </code></pre>
	 * 
	 * @note نوشتن مستند فراموش نشود.
	 * 
	 * @return promise(DmLink)
	 */
	this.link = $pluf.createGet({
		method : 'GET',
		url : '/api/dm/link/{id}'
	}, _linkCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $dm.links('paginatorParam').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(DmLink)
	 */
	this.links = $pluf.createFind({
		method : 'GET',
		url : '/api/dm/link/find',
	}, _linkCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $dm.createLink('linkData').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(DmLink)
	 */
	this.createLink = $pluf.createNew({
		method : 'POST',
		url : '/api/dm/link',
	}, _linkCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $dm.asset('assetId').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise()
	 */
	this.asset = $pluf.createGet({
		method : 'GET',
		url : '/api/dm/asset/{id}'
	}, _assetCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $dm.assets('paginatorParam').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(DmAsset)
	 */
	this.assets = $pluf.createFind({
		method : 'GET',
		url : '/api/dm/asset/find',
	}, _assetCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $dm.createAsset('assetData').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(DmAsset)
	 */
	this.createAsset = $pluf.createNew({
		method : 'POST',
		url : '/api/dm/asset/new',
	}, _assetCache);

	this.plan = $pluf.createGet({
		method : 'GET',
		url : '/api/dm/plan/{id}'
	}, _planCache);

	this.plans = $pluf.createFind({
		method : 'GET',
		url : '/api/dm/plan/find',
	}, _planCache);

	this.createPlan = $pluf.createNew({
		method : 'POST',
		url : '/api/dm/plan/new',
	}, _planCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $dm.planTemplate('planTemplateId').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(DmPlanTemplate)
	 */
	this.planTemplate = $pluf.createGet({
		method : 'GET',
		url : '/api/dm/planTemplate/{id}'
	}, _planTemplateCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $dm.planTemplates('paginatorParam').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(DmPlanTemplate)
	 */
	this.planTemplates = $pluf.createFind({
		method : 'GET',
		url : '/api/dm/planTemplate/find',
	}, _planTemplateCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $dm.createPlanTemplate('planTemplatesData')
	 * .then(function(result) { // Do something with result }); 
	 * </code></pre>
	 * 
	 * @return promise(DmPlanTemplate)
	 */
	this.createPlanTemplate = $pluf.createNew({
		method : 'POST',
		url : '/api/dm/planTemplate/new',
	}, _planTemplateCache);
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('pluf.dm')

/**
 * @memberof pluf.dm
 * @ngdoc service
 * @name $dmadmin
 * @description
 * 
 * پیاده سازی سرویس مدیریت سیستم.
 */
.service('$dmadmin', function() {

	/*
	 * این متد حجم تمام فایل هایی که کاربر می خواهد دانلود کند رو نشون میده
	 */
	this.space = function() {

	};
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('pluf.dm')
/**
 * 
 */
.service('$sdp', function($pluf, PObjectCache, SdpLink, SdpAsset, SdpAssetRelation, SdpTag, SdpCategory,
		$httpParamSerializerJQLike, $http) {

	var _linkCache = new PObjectCache(function(data) {
		return new SdpLink(data);
	});
	var _assetCache = new PObjectCache(function(data) {
		return new SdpAsset(data);
	});
	var _assetRelationCache = new PObjectCache(function(data) {
		return new SdpAssetRelation(data);
	});
	var _categoryCache = new PObjectCache(function(data) {
		return new SdpCategory(data);
	});
	var _tagCache = new PObjectCache(function(data) {
		return new SdpTag(data);
	});

	/**
	 * لینک معادل با شناسه ورودی را تعیین می‌کند
	 * 
	 * توی این پیاده سازی لینک ورودی به سرور ارسال می‌شه و نتیجه به دست
	 * آمده به صورت یک موجودیت لینک ایجاد می‌شه.
	 * 
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.link('linkId').then(function(link) {
	 * 	// Do something with link
	 * 	});
	 * </code></pre>
	 * 
	 * @note نوشتن مستند فراموش نشود.
	 * 
	 * @return promise(SdpLink)
	 */
	this.link = $pluf.createGet({
		method : 'GET',
		url : '/api/sdp/link/{id}'
	}, _linkCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.links('paginatorParam').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(SdpLink)
	 */
	this.links = $pluf.createFind({
		method : 'GET',
		url : '/api/sdp/link/find',
	}, _linkCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.createLink('assetId').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(SdpLink)
	 */
	this.createLink = $pluf.createGet({
		method : 'GET',
		url : '/api/sdp/asset/{id}/link',
	}, _linkCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.asset('assetId').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise()
	 */
	this.asset = $pluf.createGet({
		method : 'GET',
		url : '/api/sdp/asset/{id}'
	}, _assetCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.assets('paginatorParam').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(SdpAsset)
	 */
	this.assets = $pluf.createFind({
		method : 'GET',
		url : '/api/sdp/asset/find',
	}, _assetCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.createAsset('assetData').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(SdpAsset)
	 */
	this.createAsset = $pluf.createNew({
		method : 'POST',
		url : '/api/sdp/asset/new',
	}, _assetCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.createFileAsset('assetData', 'assetFile').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(SdpAsset)
	 */
	this.createFileAsset = function(objectData, file) {
		var formData = new FormData();
		for(var key in objectData){
			if(objectData[key]){
				formData.append(key, objectData[key]);
			}
		}
		formData.append('type', 'file');
		formData.append('file', file);
		return $http.post('/api/sdp/asset/new', formData, {
			transformRequest : angular.identity,
			headers : {
				'Content-Type' : undefined
			}
		})//
		.then(function(res) {
			return _assetCache.restor(res.data.id, res.data);
		});
	};

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.category('categoryId').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(SdpCategory)
	 */
	this.category = $pluf.createGet({
		method : 'GET',
		url : '/api/sdp/category/{id}'
	}, _categoryCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.categories('paginatorParam').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(PaginatorPage)
	 */
	this.categories = $pluf.createFind({
		method : 'GET',
		url : '/api/sdp/category/find',
	}, _categoryCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.createCategory('categoryData').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(SdpCategory)
	 */
	this.createCategory = $pluf.createNew({
		method : 'POST',
		url : '/api/sdp/category/new',
	}, _categoryCache);

	/**
	 * استفاده از این فراخوانی به صورت‌های زیر است:
	 * 
	 * <pre><code>
	 * $sdp.tag('tagId').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * <pre><code>
	 * $sdp.tag('tagName').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(SdpTag)
	 */
	this.tag = $pluf.createGet({
		method : 'GET',
		url : '/api/sdp/tag/{id}'
	}, _tagCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.tags('paginatorParam').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(PaginatorPage)
	 */
	this.tags = $pluf.createFind({
		method : 'GET',
		url : '/api/sdp/tag/find',
	}, _tagCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.createTag('categoryData').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(SdpTag)
	 */
	this.createTag = $pluf.createNew({
		method : 'POST',
		url : '/api/sdp/tag/new',
	}, _tagCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.assetRelation('assetRelationId').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise()
	 */
	this.assetRelation = $pluf.createGet({
		method : 'GET',
		url : '/api/sdp/assetrelation/{id}'
	}, _assetRelationCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.assetRelations('paginatorParam').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(SdpAsset)
	 */
	this.assetRelations = $pluf.createFind({
		method : 'GET',
		url : '/api/sdp/assetrelation/find',
	}, _assetRelationCache);

	/**
	 * استفاده از این فراخوانی به صورت زیر است:
	 * 
	 * <pre><code>
	 * $sdp.createAsset('assetData').then(function(result) {
	 * 	// Do something with result
	 * 	});
	 * </code></pre>
	 * 
	 * @return promise(SdpAsset)
	 */
	this.createAssetRelation = $pluf.createNew({
		method : 'POST',
		url : '/api/sdp/assetrelation/new',
	}, _assetRelationCache);
	
	/**
	 * دسته‌هایی که یک دارایی در آن‌ها قرار دارد را برمی‌گرداند.
	 * فراخوانی این تابع به صورت زیر است:
	 * <pre><code>
	 * 		$sdp.categoriesOfAsset('asset', 'pagitatorParam').then(function(result){
	 * 			// Result is a PaginatorPage<SdpCategory>
	 * 			// Do something with result.
	 * 		});
	 * </code></pre>
	 */
	this.categoriesOfAsset = function(asset, pp){
		return $pluf.createFind({
			method : 'GET',
			url : '/api/sdp/asset/'+asset.id+'/category/find',
		}, _categoryCache)(pp);
	};
	
	/**
	 * برچسب‌های زده شده روی یک دارایی را برمی‌گرداند.
	 * فراخوانی این تابع به صورت زیر است:
	 * <pre><code>
	 * 		$sdp.tagsOfAsset('asset', 'pagitatorParam').then(function(result){
	 * 			// Result is a PaginatorPage<SdpTag>
	 * 			// Do something with result.
	 * 		});
	 * </code></pre>
	 */
	this.tagsOfAsset = function(asset, pp){
		return $pluf.createFind({
			method : 'GET',
			url : '/api/sdp/asset/'+asset.id+'/tag/find',
		}, _tagCache)(pp);
	};
	
	/**
	 * دارایی‌هایی را که ب یک دارایی در ارتباط هستند (و در انتهای آن ارتباط قرار دارند) برمی‌گرداند.
	 * فراخوانی این تابع به صورت زیر است:
	 * <pre><code>
	 * 		$sdp.relatedAssetsToAsset('asset', 'pagitatorParam').then(function(result){
	 * 			// Result is a PaginatorPage<SdpAsset>
	 * 			// Do something with result.
	 * 		});
	 * </code></pre>
	 */
	this.relatedAssetsToAsset = function(asset, pp){
		return $pluf.createFind({
			method : 'GET',
			url : '/api/sdp/asset/'+asset.id+'/relation/find',
		}, _assetCache)(pp);
	};
	
	/**
	 * دارایی‌هایی که در یک دسته قرار دارد را برمی‌گرداند.
	 * فراخوانی این تابع به صورت زیر است:
	 * <pre><code>
	 * 		$sdp.assetsOfCategory('category', 'pagitatorParam').then(function(result){
	 * 			// Result is a PaginatorPage<SdpAsset>
	 * 			// Do something with result.
	 * 		});
	 * </code></pre>
	 */
	this.assetsOfCategory = function(category, pp){
		return $pluf.createFind({
			method : 'GET',
			url : '/api/sdp/category/'+category.id+'/asset/find',
		}, _assetCache)(pp);
	};
	
	/**
	 * دارایی‌هایی که برچسب داده شده روی آن‌ها زده شده است را برمی‌گرداند.
	 * فراخوانی این تابع به صورت زیر است:
	 * <pre><code>
	 * 		$sdp.assetsOfTag('tag', 'pagitatorParam').then(function(result){
	 * 			// Result is a PaginatorPage<SdpAsset>
	 * 			// Do something with result.
	 * 		});
	 * </code></pre>
	 */
	this.assetsOfTag = function(tag, pp){
		return $pluf.createFind({
			method : 'GET',
			url : '/api/sdp/tag/'+tag.id+'/asset/find',
		}, _assetCache)(pp);
	};
	
});
